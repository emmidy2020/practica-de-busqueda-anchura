#programa once
#ifndef Tools_H_
#define Tools_H_

#include<chrono>

template<class unidad, class f> <A>
double MedirTiempo(const f& funcion) {
	using namespace std::chrono;
	double Duracion;
	
	high_resolution_clock::time_point inicio, final;
	
	inicio = high_resolution_clock::now();
	funcion();
	final = high_resolution_clock::now();
	
	Duracion = duration_cast<unidad>(final-inicio).cout();
	
	return Duracion;

}
#endif // Tools_H_
