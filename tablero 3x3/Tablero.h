#pragma once
#ifndef TABLERO_H_
#define TABLERO_H_
#include<vector>
#include<array>
class Pcaballos
{
public:
	constexpr Pcaballos(std::array<int, 9>)_tablero) noexcept :tablero(_tablero) {};
	~Pcaballos() {};
	std::array<int, 9> tablero;
	std::vector<Pcaballos> GenerarMovimientos() noexcept;
	bool Comparar(Pcaballos p) { return p.tablero == tablero; }
 
private:
	constexpr void Mover(std::vector<Pcaballos>& _resultado, const int& actual, const int& op1, const int& op2) noexcept
};

std::vector<Pcaballos> PCaballos::GenerarMovimientos() noexcept {
	std::vector<Pcaballos> _resultado;

	for (size_t i = 0; i < tablero_size(); i++)
	{
		if (tablero[i] != 0)
		{
			switch (i)
			{
			case 0: {
				Mover(_resultado, i, 5, 7);
				break;
			}
			case 1: {
				Mover(_resultado, i, 6, 8);
				break;
			}
			case 2: {
				Mover(_resultado, i, 3, 7);
				break;
			}
			case 3: {
				Mover(_resultado, i, 2, 8);
				break;
			}
			case 4: {
				break;
			}
			case 5: {
				Mover(_resultado, i, 0, 6);
				break;
			}
			case 6: {
				Mover(_resultado, i, 1, 5);
				break;
			}
			case 7: {
				Mover(_resultado, i, 0, 2);
				break;
			}
			case 8: {
				Mover(_resultado, i, 1, 3);
				break;
			}
			default:
				break;
			}
		}
	}
	return _resultado;
}

constexpr void PCaballos::Mover(std::vector<Pcaballos>& _resultado, const int& actual, const int& op1, const int& op2) noexcept
{
	auto SWP - [&](std::array<int, 9> _tablero, const int& actual, const int& nuevo)
	{
		_tablero[nuevo] = _tablero[actual];
		_tablero[actual] = 0;
		_resultado.push_back(PCaballos(_tablero));
	};

	if (tablero[op1] == 0)
		SWP(tablero, actual, op1);
	if (tablero[op2] == 0)
		SWP(tablero, actual, op2);
}
#endif // !TABLERO_H_
