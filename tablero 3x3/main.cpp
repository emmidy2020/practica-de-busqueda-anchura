#include<iostream>
#include"Tablero.h"
#include"busqueda.h"
#include"tools.h"

int main{}{
    std::vector<PCaballos> resultado;
    double duracion;
    PCaballos problema{{
        2,0,2,
        0,0,0,
        1,0,1
    }};
    PCaballos solucion{{
        1,0,1,
        0,0,0,
        2,0,2
    }};
  

    duracion = MedirTiempo<std::chrono::milliseconds>([&]{
        resultado = BusquedaAnchura<PCaballos>(problema,solucion);
        //resultado = BusquedaProfundidad<PCaballos>(problema,solucion);
    });

    for (size_t i = 0; i < resultado.size(); i++)
    {
        std::cout << "(" << i << ")" << std::endl;
        for (size_t j = 0; j < 9; j++)
        {
            std::cout <<resultados[i].tablero[j] <<"";
            if (j == 2 || j ==5)
            std::cout << std::endl;
        }
        std::cout<< "\n\n";
    }
    std::cout<<"Tiempo: " <<duracion<<"ms";
}