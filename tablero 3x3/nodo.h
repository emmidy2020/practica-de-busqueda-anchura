#ifndef NODO_H_
#define NODO_H_
#include<vector>
#include <algorithm>

template<typename T>
concept NReq = requires (T t){
    t.GenerarMovimientos() == std::vector<T>();
    t.Comparar(t)  == true;
};

template<class T> requires NReq<T>
    struct
    Nodo{
    Nodo(T dato, Nodo<T>* padre){
        Datos = dato;
        Padre = Padre;
        if (Padre != nullptr)
            profundidad = padre -> profundidad + 1;
            else
            profundidad = 0;
        }
        Nodo<T>* Padre;
        T Datos;
        int profundidad;
    };
    #endif//!NODO_H_