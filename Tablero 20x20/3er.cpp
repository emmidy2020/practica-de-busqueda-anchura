#include <iostream>
using std::cout;
using std::endl;
 
#include <iomanip>
using std::setw;
 
int heuristica(int, int, const int[][8], const int[][8]);
void modificarHeuristica(const int[][8], int[][8]);
 
int main()
{
  int movimiento;
  int horizontal[] = {2, 1, -1, -2, -2, -1, 1, 2};
  int vertical[] = {-1, -2, -2, -1, 1, 2, 2, 1};
  int tablero[8][8] = {{},{}};
  int accesibilidad[8][8] = {{2, 3, 4, 4, 4, 4, 5, 2},
                 {3, 4, 6, 6, 6, 6, 4, 3},
                 {4, 6, 8, 8, 8, 8, 6, 4},
                 {4, 6, 8, 8, 8, 8, 6, 4},
                 {4, 6, 8, 8, 8, 8, 6, 4},
                 {4, 6, 8, 8, 8, 8, 6, 4},
                 {3, 4, 6, 6, 6, 6, 4, 3},
                 {2, 3, 4, 4, 4, 4, 5, 2}};
  
  int filaActual = 3;
  int columnaActual = 4;
  int pasos = 2;
  
  tablero[filaActual][columnaActual] = 1;
  
  modificarHeuristica(tablero, accesibilidad);
  
  while(heuristica(filaActual, columnaActual, tablero, accesibilidad) != -1)
  {
    movimiento = heuristica(filaActual, columnaActual, tablero, accesibilidad);
    filaActual += vertical[movimiento];
    columnaActual += horizontal[movimiento];
    tablero[filaActual][columnaActual] = pasos;
    modificarHeuristica(tablero, accesibilidad);
    pasos++;
  }
  
  for(int x = 0; x < 8; x++)
  {
    for(int y = 0; y < 8; y++)
    {
      cout << setw(3) << tablero[x][y];
    }
    cout << endl;
  }
  cout << endl;
  return 0;
}
 
void modificarHeuristica(const int tablero[][8], int nuevo[][8])
{
  for(int x = 0; x < 8; x++)
  {
    int movimientos = 0;
    
    for(int y = 0; y < 8; y++)
    {
      if((x - 1 > -1) && (x - 1 < 8) && (y + 2 > -1) && (y + 2 < 8) && (tablero[x-1][y+2] == 0))
    movimientos++;
      if((x - 2 > -1) && (x - 2 < 8) && (y + 1 > -1) && (y + 1 < 8) && (tablero[x-2][y+1] == 0))
    movimientos++;
      if((x - 2 > -1) && (x - 2 < 8) && (y - 1 > -1) && (y - 1 < 8) && (tablero[x-2][y-1] == 0))
    movimientos++;
      if((x - 1 > -1) && (x - 1 < 8) && (y - 2 > -1) && (y - 2 < 8) && (tablero[x-1][y-2] == 0))
    movimientos++;
      if((x + 1 > -1) && (x + 1 < 8) && (y - 2 > -1) && (y - 2 < 8) && (tablero[x+1][y-2] == 0))
    movimientos++;
      if((x + 2 > -1) && (x + 2 < 8) && (y - 1 > -1) && (y - 1 < 8) && (tablero[x+2][y-1] == 0))
    movimientos++;
      if((x + 2 > -1) && (x + 2 < 8) && (y + 1 > -1) && (y + 1 < 8) && (tablero[x+2][y+1] == 0))
    movimientos++;
      if((x + 1 > -1) && (x + 1 < 8) && (y + 2 > -1) && (y + 2 < 8) && (tablero[x+1][y+2] == 0))
    movimientos++;
      
      nuevo[x][y] = movimientos;
    }
  }
}
 
int heuristica(int x, int y, const int tablero[][8], const int tableroH[][8])
{
  int menor = 10;
  
    if((x - 1 > -1) && (x - 1 < 8) && (y + 2 > -1) && (y + 2 < 8) && (tablero[x-1][y+2] == 0))
      if(tableroH[x-1][y+2] < menor)
    menor = 0;
    if((x - 2 > -1) && (x - 2 < 8) && (y + 1 > -1) && (y + 1 < 8) && (tablero[x-2][y+1] == 0))
      if(tableroH[x-2][y+1] < menor)
    menor = 1;
    if((x - 2 > -1) && (x - 2 < 8) && (y - 1 > -1) && (y - 1 < 8) && (tablero[x-2][y-1] == 0))
      if(tableroH[x-2][y-1] < menor)
    menor = 2;
    if((x - 1 > -1) && (x - 1 < 8) && (y - 2 > -1) && (y - 2 < 8) && (tablero[x-1][y-2] == 0))
      if(tableroH[x-1][y-2] < menor)
    menor = 3;
    if((x + 1 > -1) && (x + 1 < 8) && (y - 2 > -1) && (y - 2 < 8) && (tablero[x+1][y-2] == 0))
      if(tableroH[x+1][y-2] < menor)
    menor = 4;
    if((x + 2 > -1) && (x + 2 < 8) && (y - 1 > -1) && (y - 1 < 8) && (tablero[x+2][y-1] == 0))
      if(tableroH[x+2][y-1] < menor)
    menor = 5;
    if((x + 2 > -1) && (x + 2 < 8) && (y + 1 > -1) && (y + 1 < 8) && (tablero[x+2][y+1] == 0))
      if(tableroH[x+2][y+1] < menor)
    menor = 6;
    if((x + 1 > -1) && (x + 1 < 8) && (y + 2 > -1) && (y + 2 < 8) && (tablero[x+1][y+2] == 0))
      if(tableroH[x+1][y+2] < menor)
    menor = 7;
    
    if(menor == 10)
      return -1;
    else
      return menor;    
}
